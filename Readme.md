Centos 3.9 image for oracle 8.1.7.

This vagrantfile loads a specialized centos image for running oracle 8.1.7 (32 bit) on linux centos 3.9 (32 bit).
The vagrant box does NOT contain ANY oracle software.
Usage is at your own risk.
The centos installation is modified to work with the oracle software, which means a forced glibc 2.1 installation and some changed binaries.

The operating system centos 3.9 is so old that it doesn't work well with virtualbox and vagrant.
* Virtualbox: the VM will not cleanly shutdown. A shutdown in linux will keep the VM running. A shutdown with vagrant will force shutdown (IMMEDIATE).
* Vagrant: after the VM has startup, vagrant will not return the prompt because it tries to login which it can't because of too old SSH cyphers. So you have to terminate vagrant after you are sure it has started (ctrl-c).
This also means that the vagrant provisioners will not run, you have to do that by hand.

In order to use the oracle database version 8.1.7, perform the following actions:

```shell
sftp -c 3des-cbc -o KexAlgorithms=diffie-hellman-group1-sha1 root@192.168.66.37
cd /tmp
mput oracle_817_installation.tgz
mput database_setup.tgz
exit
```

(oracle_817_installation.tgz contains the full database installation, database_setup.tgz contains scripts to create the database)

```shell
ssh -c 3des-cbc -o KexAlgorithms=diffie-hellman-group1-sha1 root@192.168.66.37
tar xzf /tmp/oracle_817_installation.tgz -C /
tar xzf /tmp/database_setup.tgz -C /
su - oracle
orapwd file=$ORACLE_HOME/dbs/orapw password=change_on_install
sh $ORACLE_HOME/assistants/dbca/o817_installer
```
